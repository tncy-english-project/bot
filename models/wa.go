package models

import (
	"context"

	"github.com/edgedb/edgedb-go"
	"gitlab.com/tncy-english-project/bot/config"
)

type WordAssociation struct {
	GameID          string                  `edgedb:"game_id"`
	User            User                    `edgedb:"user"`
	Finished        bool                    `edgedb:"finished"`
	Word            string                  `edgedb:"word"`
	ProvidedAnswers []WordAssociationAnswer `edgedb:"provided_answers"`
	CorrectAnswers  []WordAssociationAnswer `edgedb:"correct_answers"`
}

func (w *WordAssociation) GetGameID() string {
	return w.GameID
}

func (w *WordAssociation) GetUser() User {
	return w.User
}

func (w *WordAssociation) Create() error {
	q1 := `
		INSERT default::WordAssociationAnswer {
			game_id := <str>$game_id,
			word := <str>$word,
			proximity := <float64>$proximity,
		}
	`

	for _, answer := range w.CorrectAnswers {
		a := map[string]interface{}{
			"game_id":   w.GameID,
			"word":      answer.Word,
			"proximity": answer.Proximity,
		}

		if err := config.Client.Execute(context.Background(), q1, a); err != nil {
			return err
		}
	}

	q2 := `
		INSERT default::WordAssociation {
			game_id := <str>$game_id,
			user := (SELECT default::User FILTER .discord_id = <str>$discord_id),
			word := <str>$word,
			correct_answers := (
				SELECT default::WordAssociationAnswer
				FILTER .game_id = <str>$game_id
			)
		}
	`

	a := map[string]interface{}{
		"game_id":    w.GameID,
		"discord_id": w.User.DiscordID,
		"word":       w.Word,
	}

	if err := config.Client.Execute(context.Background(), q2, a); err != nil {
		return err
	}

	return nil
}

func (w *WordAssociation) Delete() error {
	q := `
		DELETE default::WordAssociation
		FILTER .game_id = <str>$game_id
	`

	a := map[string]interface{}{
		"game_id": w.GameID,
	}

	if err := config.Client.Execute(context.Background(), q, a); err != nil {
		return err
	}

	q = `
		DELETE default::WordAssociationAnswer
		FILTER .game_id = <str>$game_id
	`

	if err := config.Client.Execute(context.Background(), q, a); err != nil {
		return err
	}

	return nil
}

func (w *WordAssociation) Update() error {
	// Update provided answers
	q := `
		UPDATE default::WordAssociation
		FILTER .game_id = <str>$game_id
		SET {
			provided_answers := <array<default::WordAssociationAnswer>>$provided_answers
		}
	`

	a := map[string]interface{}{
		"game_id":          w.GameID,
		"provided_answers": w.ProvidedAnswers,
	}

	if err := config.Client.Execute(context.Background(), q, a); err != nil {
		return err
	}

	return nil
}

type WordAssociationAnswer struct {
	GameID    string                 `edgedb:"game_id"`
	Word      string                 `edgedb:"word"`
	Proximity edgedb.OptionalFloat64 `edgedb:"proximity"`
}
