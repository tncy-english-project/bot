package models

import (
	"context"

	"github.com/edgedb/edgedb-go"
	"gitlab.com/tncy-english-project/bot/config"
)

// Here is the schema:

// property wa_points -> int64;
// property wa_games_played -> int64;
// property quiz_points -> int64;
// property quiz_games_played -> int64;
// property wordle_points -> int64;
// property wordle_games_played -> int64;
// property lyrics_points -> int64;

type User struct {
	DiscordID    string               `edgedb:"discord_id"`
	WAPoints     edgedb.OptionalInt64 `edgedb:"wa_points"`
	WAPlayed     edgedb.OptionalInt64 `edgedb:"wa_games_played"`
	QuizPoints   edgedb.OptionalInt64 `edgedb:"quiz_points"`
	QuizPlayed   edgedb.OptionalInt64 `edgedb:"quiz_games_played"`
	WordlePoints edgedb.OptionalInt64 `edgedb:"wordle_points"`
	WordlePlayed edgedb.OptionalInt64 `edgedb:"wordle_games_played"`
	LyricsPoints edgedb.OptionalInt64 `edgedb:"lyrics_points"`
}

func GetUser(discordID string) (*User, error) {
	q := `
		SELECT default::User {
			discord_id,
			wa_points,
			wa_games_played,
			quiz_points,
			quiz_games_played,
			wordle_points,
			wordle_games_played,
			lyrics_points
		}
		FILTER .discord_id = <str>$discordID
		LIMIT 1
	`
	a := map[string]interface{}{
		"discordID": discordID,
	}

	var user User
	err := config.Client.QuerySingle(context.Background(), q, &user, a)
	if err != nil {
		user := &User{
			DiscordID: discordID,
		}

		if err := user.Create(); err != nil {
			return nil, err
		}

		return user, nil
	}

	return &user, nil
}

func (u *User) Update() error {
	q := `
		UPDATE default::User
		FILTER .discord_id = <str>$discord_id
		SET {
			wa_points := <int64>$wa_points,
			wa_games_played := <int64>$wa_games_played,
			quiz_points := <int64>$quiz_points,
			quiz_games_played := <int64>$quiz_games_played,
			wordle_points := <int64>$wordle_points,
			wordle_games_played := <int64>$wordle_games_played,
			lyrics_points := <int64>$lyrics_points
		}
	`

	waPoints, _ := u.WAPoints.Get()
	waPlayed, _ := u.WAPlayed.Get()
	quizPoints, _ := u.QuizPoints.Get()
	quizPlayed, _ := u.QuizPlayed.Get()
	wordlePoints, _ := u.WordlePoints.Get()
	wordlePlayed, _ := u.WordlePlayed.Get()
	lyricsPoints, _ := u.LyricsPoints.Get()

	a := map[string]interface{}{
		"discord_id":          u.DiscordID,
		"wa_points":           waPoints,
		"wa_games_played":     waPlayed,
		"quiz_points":         quizPoints,
		"quiz_games_played":   quizPlayed,
		"wordle_points":       wordlePoints,
		"wordle_games_played": wordlePlayed,
		"lyrics_points":       lyricsPoints,
	}

	if err := config.Client.Execute(context.Background(), q, a); err != nil {
		return err
	}

	return nil
}

func (u *User) Create() error {
	q := `
		INSERT default::User {
			discord_id := <str>$discord_id
		}
	`

	a := map[string]interface{}{
		"discord_id": u.DiscordID,
	}

	if err := config.Client.Execute(context.Background(), q, a); err != nil {
		return err
	}

	return nil
}

func (u *User) Delete() error {
	q := `
		DELETE default::User
		FILTER .discord_id = <str>$discord_id
	`
	a := map[string]interface{}{
		"discord_id": u.DiscordID,
	}

	if err := config.Client.Execute(context.Background(), q, a); err != nil {
		return err
	}

	return nil
}

func (u *User) AddWaPoints(a int64) error {
	waPoints, _ := u.WAPoints.Get()
	u.WAPoints.Set(waPoints + a)

	return u.Update()
}

func (u *User) AddWaPlayed(a int64) error {
	waPlayed, _ := u.WAPlayed.Get()
	u.WAPlayed.Set(waPlayed + a)

	return u.Update()
}

func (u *User) AddQuizPoints(a int64) error {
	quizPoints, _ := u.QuizPoints.Get()
	u.QuizPoints.Set(quizPoints + a)

	return u.Update()
}

func (u *User) AddQuizPlayed(a int64) error {
	quizPlayed, _ := u.QuizPlayed.Get()
	u.QuizPlayed.Set(quizPlayed + a)

	return u.Update()
}

func (u *User) AddWordlePoints(a int64) error {
	wordlePoints, _ := u.WordlePoints.Get()
	u.WordlePoints.Set(wordlePoints + a)

	return u.Update()
}

func (u *User) AddWordlePlayed(a int64) error {
	wordlePlayed, _ := u.WordlePlayed.Get()
	u.WordlePlayed.Set(wordlePlayed + a)

	return u.Update()
}

func (u *User) AddLyricsPoints(a int64) error {
	lyricsPoints, _ := u.LyricsPoints.Get()
	u.LyricsPoints.Set(lyricsPoints + a)

	return u.Update()
}
