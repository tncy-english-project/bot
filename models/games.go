package models

type Game interface {
	GetGameID() string
	GetUser() User
}
