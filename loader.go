package main

import (
	cmd_game "gitlab.com/tncy-english-project/bot/commands/game"
	cmd_profile "gitlab.com/tncy-english-project/bot/commands/profile"
	cmd_system "gitlab.com/tncy-english-project/bot/commands/system"
	"gitlab.com/tncy-english-project/bot/discord"
)

func StartDiscord() {
	discord.Start()

	discord.AddCmd(cmd_system.PingComand)
	discord.AddCmd(cmd_game.WAComand)
	discord.AddCmd(cmd_profile.ProfileCommand)
	discord.AddCmd(cmd_game.QuizCommand)
	discord.AddCmd(cmd_game.TTSCommand)
	discord.AddCmd(cmd_game.TTSMessageCommand)
	discord.AddCmd(cmd_game.GPTCommand)
	discord.AddCmd(cmd_game.TranslateCommand)
	discord.AddCmd(cmd_game.TranslateMessageCommand)
	discord.AddCmd(cmd_game.WordleCommand)
	discord.AddCmd(cmd_game.DefineCommand)
	discord.AddCmd(cmd_game.ObjectsCommand)
	discord.AddCmd(cmd_game.LyricsCommand)

	// For the help command
	discord.MakeEmbed()

	// Load them and registers them
	discord.CommandRouter.LoadSlashCommands(discord.Session)
}
