package discord

import (
	"github.com/bwmarrin/discordgo"
)

type CommandArg struct {
	Name  string
	Value interface{}
	Found bool
}

type Args []*CommandArg

type Arg struct {
	Name        string
	Description string
	Size        int
	Required    bool
	Choices     []*Choice
	Type        discordgo.ApplicationCommandOptionType
}

func (a *Args) GetArg(name string, index int, def interface{}) (argument *CommandArg) {
	s := *a
	argument = &CommandArg{
		Name:  name,
		Value: def,
		Found: false,
	}
	for i, arg := range s {
		if arg.Name == name {
			arg.Found = true
			return arg
		}
		if i == index {
			arg.Found = true
			argument = arg
		}
	}
	return
}
