package discord

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"runtime"
	"time"

	"github.com/bwmarrin/discordgo"
)

func Start() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UnixNano())

	CommandRouter = router{
		Prefix:         "$",
		ListenerPrefix: ">",
		RateLimit:      2000,
		Commands:       make(map[string][]*Command),
	}

	session, err := discordgo.New("Bot " + os.Getenv("TOKEN"))
	if err != nil {
		panic(err)
	}
	Session = session
	Session.Identify.Intents = discordgo.IntentsAll
	session.StateEnabled = true

	session.AddHandler(routeMessages)
	session.AddHandler(routeInteraction)
	session.AddHandler(routeComponents)
	session.AddHandler(routeModals)
	session.AddHandler(botReady)

	log.Println("Starting the shard manager")
	err = session.Open()
	if err != nil {
		log.Fatal("Failed to start: ", err)
	}

	time.Sleep(1 * time.Second)

	DefaultFooter = &discordgo.MessageEmbedFooter{
		IconURL: session.State.User.AvatarURL("256"),
		Text:    "Fait avec ❤️ par Yewolf et ELFamoso - 2023",
	}
}

var messages = []string{
	"Je s'appelle Groot",
	"Qui suis-je ?",
	"La distance entre la lune et la terre est de 384 400 km",
	"Je suis un bot",
	"Qui est le meilleur ?",
	"Faites un don à la fondation Yewolf",
}

func botReady(s *discordgo.Session, evt *discordgo.Ready) {
	ticker := time.NewTicker(5 * time.Minute)
	done := make(chan bool)

	go func() {
		i := 0
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				s.UpdateGameStatus(0, fmt.Sprintf("%s (/help)", messages[i]))
				// Next message
				i++
				if i >= len(messages) {
					i = 0
				}
			}
		}
	}()
}
