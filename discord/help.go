package discord

import (
	"fmt"
	"sort"

	"github.com/bwmarrin/discordgo"
)

type menuName struct {
	Name      string
	EmojiName string
	EmojiID   string
}

var (
	GeneralMenu = menuName{
		Name:      "Générale",
		EmojiName: "🖥️",
	}
	GameMenu = menuName{
		Name:      "Jeux",
		EmojiName: "🎮",
	}
)

var HelpMenus map[menuName][]*Command
var MenuEmbed map[menuName]*discordgo.MessageEmbed

func MakeEmbed() {
	MenuEmbed = make(map[menuName]*discordgo.MessageEmbed)
	for menuName, cmds := range HelpMenus {
		embed := &discordgo.MessageEmbed{
			Title: fmt.Sprintf("Commandes %s", GeneralMenu.Name),
		}
		for i, cmd := range cmds {
			if i != 0 {
				embed.Description += "\n"
			}
			embed.Description += fmt.Sprintf("`%s%s : %s`", CommandRouter.Prefix, cmd.HelpName, cmd.Description)
		}
		MenuEmbed[menuName] = embed
	}
}

func HelpComponent(menuID string, defaultMenu string) []discordgo.MessageComponent {
	var opts []discordgo.SelectMenuOption

	var keys []menuName
	for k := range MenuEmbed {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i, j int) bool {
		return keys[i].Name < keys[j].Name
	})

	for _, menu := range keys {
		if menu.Name == "" {
			continue
		}
		opt := discordgo.SelectMenuOption{
			Label: menu.Name,
			Value: menu.Name,
			Emoji: discordgo.ComponentEmoji{
				Name: menu.EmojiName,
				ID:   menu.EmojiID,
			},
		}
		if menu.Name == defaultMenu {
			opt.Default = true
		}
		opts = append(opts, opt)
	}

	cmp := []discordgo.MessageComponent{
		&discordgo.ActionsRow{
			Components: []discordgo.MessageComponent{
				&discordgo.SelectMenu{
					CustomID: menuID,
					Options:  opts,
				},
			},
		},
	}

	return cmp
}
