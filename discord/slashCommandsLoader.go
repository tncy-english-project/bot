package discord

import (
	"fmt"
	"os"

	"github.com/bwmarrin/discordgo"
)

func choiceConvert(c []*Choice) (r []*discordgo.ApplicationCommandOptionChoice) {
	for _, choice := range c {
		r = append(r, &discordgo.ApplicationCommandOptionChoice{
			Name:  choice.Name,
			Value: choice.Value,
		})
	}
	return
}

func (c *Command) makeOption() (opts []*discordgo.ApplicationCommandOption) {
	if len(c.SubCommands) == 0 {
		for _, arg := range c.Args {
			opt := &discordgo.ApplicationCommandOption{
				Name:        arg.Name,
				Description: arg.Description,
				Type:        arg.Type,
				Required:    arg.Required,
				Choices:     choiceConvert(arg.Choices),
			}
			opts = append(opts, opt)
		}
	} else {
		for _, sub := range c.SubCommands {
			opt := &discordgo.ApplicationCommandOption{
				Name:        sub.Name,
				Description: sub.Description,
				Type:        discordgo.ApplicationCommandOptionSubCommand,
				Options:     sub.makeOption(),
			}
			opts = append(opts, opt)
		}
	}
	return
}

func (c *Command) make() *discordgo.ApplicationCommand {
	return &discordgo.ApplicationCommand{
		Name:        c.Name,
		Type:        c.Type,
		Description: c.Description,
		Options:     c.makeOption(),
	}
}

func (r *router) LoadSlashCommands(s *discordgo.Session) {
	// On enregistre les commandes par guild
	guildID := ""
	guildCmds, _ := s.ApplicationCommands(os.Getenv("APP_ID"), guildID)
	for _, gcmd := range guildCmds {
		// On vérifie les commandes existantes (pour éviter d'overwrite)
		remove := true
		for _, botcmd := range r.Commands[guildID] {
			if IsCommandEqual(gcmd, botcmd.make()) {
				remove = false
				break
			}
		}
		if remove {
			s.ApplicationCommandDelete(os.Getenv("APP_ID"), guildID, gcmd.ID)
			fmt.Printf("Removed '%v' for '%s' \n", gcmd.Name, guildID)
		}
	}
	for _, realcmd := range r.Commands[guildID] {
		// On ajoute les manquantes
		add := true
		for _, dcmd := range guildCmds {
			if IsCommandEqual(dcmd, realcmd.make()) {
				add = false
				break
			}
		}
		if add {
			_, err := s.ApplicationCommandCreate(os.Getenv("APP_ID"), guildID, realcmd.make())
			if err != nil {
				fmt.Printf("Cannot create '%v' : %v\n", realcmd.Name, err.Error())
			} else {
				fmt.Printf("Created '%v' for '%s' \n", realcmd.Name, guildID)
			}
		}
	}
}

func isOptionEqual(c1, c2 *discordgo.ApplicationCommandOption) (b bool) {
	if c1.Name != c2.Name {
		return
	}
	if c1.Description != c2.Description {
		return
	}
	if c1.Type != c2.Type {
		return
	}
	if len(c1.Options) != len(c2.Options) {
		return
	}
	if len(c1.Choices) != len(c2.Choices) {
		return
	}
	if c1.Autocomplete != c2.Autocomplete {
		return
	}
	if c1.Required != c2.Required {
		return
	}
	if len(c1.ChannelTypes) != len(c2.ChannelTypes) {
		return
	}
	for i, chan1 := range c1.ChannelTypes {
		chan2 := c2.ChannelTypes[i]
		if chan1 != chan2 {
			return
		}
	}
	for i, opt1 := range c1.Options {
		opt2 := c2.Options[i]
		if !isOptionEqual(opt1, opt2) {
			return
		}
	}
	return true
}

func IsCommandEqual(c1, c2 *discordgo.ApplicationCommand) (b bool) {
	if c1.Name != c2.Name {
		return
	}
	if c1.Description != c2.Description {
		return
	}
	if c1.Type != c2.Type {
		return
	}
	if len(c1.Options) != len(c2.Options) {
		return
	}
	for i, opt1 := range c1.Options {
		opt2 := c2.Options[i]
		if !isOptionEqual(opt1, opt2) {
			return
		}
	}
	return true
}
