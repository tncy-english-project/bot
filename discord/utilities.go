package discord

import (
	"fmt"
	"math"
	"time"

	"github.com/bwmarrin/discordgo"
)

type TokenData struct {
	AuthorID string
	GuildID  string
}

func UnixFormatter(timestamp int64) string {
	t := timestamp - time.Now().Unix()
	H := int(float64(t) / float64(3600))
	M := int(math.Mod(float64(t)/float64(60), 60))
	S := int(math.Mod(float64(t), 60))

	return fmt.Sprintf("**%02d hours %02d minutes %02d seconds**", H, M, S)
}

func UnixNanoFormatter(timestamp int64) string {
	t := timestamp - time.Now().UnixNano()
	H := int(float64(t) / float64(3600))
	M := int(math.Mod(float64(t)/float64(60), 60))
	S := int(math.Mod(float64(t), 60))

	return fmt.Sprintf("**%02d hours %02d minutes %02d seconds**", H, M, S)
}

func GetYear(promotion int) int {
	date := time.Now()
	curYear := date.Year()
	diff := curYear - promotion + 3

	yearCut := time.Date(curYear, 7, 1, 0, 0, 0, 0, time.Local)

	if date.Sub(yearCut) < 0 {
		return diff
	} else {
		return diff + 1
	}
}

func (ctx *CmdContext) IsRoleMissing(query string, guildID string) (r *discordgo.Role, missing bool) {
	roles, _ := ctx.Session.GuildRoles(ctx.GuildID)
	for _, role := range roles {
		// Name or ID
		if role.Name != query && role.ID != query {
			continue
		}
		r = role
		for _, roleID := range ctx.Roles {
			if roleID != role.ID {
				continue
			}
			return
		}
		break
	}
	return r, true
}
