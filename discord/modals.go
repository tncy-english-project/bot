package discord

import (
	"gitlab.com/tncy-english-project/bot/models"

	"github.com/bwmarrin/discordgo"
)

func routeModals(s *discordgo.Session, interaction *discordgo.InteractionCreate) {
	if interaction.Type != discordgo.InteractionModalSubmit {
		return
	}
	id := interaction.ModalSubmitData().CustomID
	val, found := ActiveMenus.Get(id)
	if !found {
		return
	}
	m := val.(*Menus)
	ctx := &CmdContext{
		Session:   s,
		ID:        interaction.ID,
		ChannelID: interaction.ChannelID,
		GuildID:   interaction.GuildID,

		IsComponent:   true,
		IsInteraction: true,
		Menu:          m,
		ModalData:     interaction.ModalSubmitData(),
		Interaction:   interaction.Interaction,
		Message:       interaction.Interaction.Message,
	}
	if interaction.Member != nil {
		ctx.Author = interaction.Member.User
	} else {
		ctx.Author = interaction.User
	}

	ctx.User, _ = models.GetUser(ctx.Author.ID)

	m.MenuID = id

	s.InteractionRespond(ctx.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseDeferredMessageUpdate,
	})

	m.Call(ctx)
}
