package discord

import (
	"os"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/bwmarrin/snowflake"
	"github.com/pmylund/go-cache"
)

// Useful
var StartTime = time.Now()

// DGO
var Session *discordgo.Session
var CommandRouter router
var DefaultFooter *discordgo.MessageEmbedFooter

// Caches
var RateLimitCache *cache.Cache
var ActiveMenus *cache.Cache
var ListenersCache *cache.Cache
var TokenCache *cache.Cache

// UUID
var Node *snowflake.Node

var CookieSecret = os.Getenv("LISTENER_SECRET")

func init() {
	RateLimitCache = cache.New(5*time.Minute, 10*time.Minute)
	ActiveMenus = cache.New(15*time.Minute, 10*time.Minute)
	ListenersCache = cache.New(5*time.Minute, 10*time.Minute)
	TokenCache = cache.New(35*time.Minute, 60*time.Minute)
	HelpMenus = make(map[menuName][]*Command)
	Node, _ = snowflake.NewNode(1)
}
