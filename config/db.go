package config

import (
	"context"
	"log"

	"github.com/edgedb/edgedb-go"
)

var Client *edgedb.Client

func init() {
	ctx := context.Background()
	client, err := edgedb.CreateClient(ctx, edgedb.Options{
		Port:        5656,
		TLSSecurity: "insecure",
	})
	if err != nil {
		log.Fatal(err)
	}

	Client = client
}
