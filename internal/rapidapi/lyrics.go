package rapidapi

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type LyricsApiResponse struct {
	Message struct {
		Header struct {
			StatusCode  int     `json:"status_code"`
			ExecuteTime float64 `json:"execute_time"`
		} `json:"header"`
		Body struct {
			Lyrics struct {
				LyricsID          int    `json:"lyrics_id"`
				Restricted        int    `json:"restricted"`
				Instrumental      int    `json:"instrumental"`
				LyricsBody        string `json:"lyrics_body"`
				LyricsLanguage    string `json:"lyrics_language"`
				ScriptTrackingURL string `json:"script_tracking_url"`
				PixelTrackingURL  string `json:"pixel_tracking_url"`
				LyricsCopyright   string `json:"lyrics_copyright"`
				BacklinkURL       string `json:"backlink_url"`
				UpdatedTime       string `json:"updated_time"`
			} `json:"lyrics"`
		} `json:"body"`
	} `json:"message"`
}

func GetLyrics(id string) (*LyricsApiResponse, error) {
	url := "https://api.musixmatch.com/ws/1.1/track.lyrics.get?apikey=" + os.Getenv("MUSIXMATCH_API") + "&track_id=" + id

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var response LyricsApiResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

func FakeLyrics(id string) (*LyricsApiResponse, error) {
	var body = []byte(`{"message":{"header":{"status_code":200,"execute_time":0.012667179107666},"body":{"lyrics":{"lyrics_id":31305063,"explicit":0,"lyrics_body":"We were good, we were gold\nKinda dream that can't be sold\nWe were right 'til we weren't\nBuilt a home and watched it burn\n\nMmm, I didn't wanna leave you\nI didn't wanna lie\nStarted to cry, but then remembered I\n\nI can buy myself flowers\nWrite my name in the sand\nTalk to myself for hours\nSay things you don't understand\nI can take myself dancing\nAnd I can hold my own hand\nYeah, I can love me better than you can\n\nCan love me better\nI can love me better, baby\nCan love me better\nI can love me better, baby\n\nPaint my nails cherry red\n...\n\n******* This Lyrics is NOT for Commercial use *******","script_tracking_url":"https:\/\/tracking.musixmatch.com\/t1.0\/m_js\/e_1\/sn_0\/l_31305063\/su_0\/rs_0\/tr_3vUCAHR4Y-fEXxj7dAr_ws1MXHFbrco_X5Qi5SZlj3we5DW8XYZyM6C12pZGjz-2VcHzVnGBoC7HYoif1_tZomMakq6zARbCxokkidH2h4uBjo4EVniBkGxyoufSbMFXghFh0moe_fRdRWPTjWwJ4IXu27AEf2Nkm3tYm5o4ZPGCAUrRJrrGcGlUxeuJqpN2kAqkSHmy7xALGiS1XWQRofTvx6_ZUaUco7FJKtLt7KDUPvK4d9S-wlR_dNG05cgYMJeFcIJtAWz73UaudOzzd83NOsofjJ7leu9yQsxDRPaLCdRTshz-9KZFgzLBebmrfNDCB9n3v_QmzumYh6RvGfkzp8nXj8TyOwQTr9-rU96vUwDcsFHjLVnBrtntZUQZh6vdtYwoXToHL--BQawejAGtmRCsIlZyGlo\/","pixel_tracking_url":"https:\/\/tracking.musixmatch.com\/t1.0\/m_img\/e_1\/sn_0\/l_31305063\/su_0\/rs_0\/tr_3vUCAOSuGiHPOl6FzkcF--0fuZi7V2FFCkgIFUyx2pGEa2zPWg6tPS9mJkYU4R32tc8nwp9lFYtj0yptgvaWVU4yLwMAO1KKec4-FUGM5XJC1BKEQPmQVkFbvFFLhczYWIQL3xoU7-mgS-8BSEfBqB3vhBuXhiTE4XHUN_fKJHsNEpXhDkj7DThwapRsIykAy8jB0odZMe9ULEhT5emyy4hxm4aRt_Li6uqWFstU0gglRPsY7ymoU3i4GZqPqIgWbBwUeCQJ5E07IVN8sKvJkIjUJRDrmdz2xhhIRnDuq7Xxvz9EqhLemPyxCDpGasqRzIZKoviof43CRdaFtyD2pzPdrQ-Q1cC68ZBR7FBRsrpZbNKLnx1PN0dR4SE3zRb4hGq0FOFT1odxrTdQTzNupDdv5rj4UjlqVcA\/","lyrics_copyright":"Lyrics powered by www.musixmatch.com. This Lyrics is NOT for Commercial use and only 30% of the lyrics are returned.","updated_time":"2023-03-22T06:54:48Z"}}}}`)

	var response LyricsApiResponse
	err := json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}

type TranslationApiResponse struct {
	Data struct {
		TranslatedText string `json:"translatedText"`
	} `json:"data"`
}

func TranslateLyrics(l *LyricsApiResponse) (*TranslationApiResponse, error) {
	data := strings.ReplaceAll(l.Message.Body.Lyrics.LyricsBody, "\n", " \n ")
	data = strings.ReplaceAll(data, "\r", " \r ")
	data = strings.ReplaceAll(data, "******* This Lyrics is NOT for Commercial use *******", "")
	data = url.QueryEscape(data)

	url := "https://text-translator2.p.rapidapi.com/translate"

	payload := strings.NewReader("source_language=en&target_language=fr&text=" + data)

	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
	req.Header.Add("X-RapidAPI-Host", "text-translator2.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var response TranslationApiResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}

	return &response, nil
}
