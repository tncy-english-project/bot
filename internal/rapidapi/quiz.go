package rapidapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
)

type Quiz struct {
	Area      string     `json:"area"`
	Author    string     `json:"author"`
	Email     string     `json:"email"`
	Level     int        `json:"level"`
	QuizList  []QuizList `json:"quizlist"`
	ResultMsg string     `json:"result_msg"`
	Version   string     `json:"version"`
}

type QuizList struct {
	Correct int      `json:"correct"`
	Option  []string `json:"option"`
	Quiz    []string `json:"quiz"`
}

// es, ms, hs, ksat, toeic, toefl, teps, sat, ielts, gre, gmat, overall
var validAreas = []string{"es", "ms", "hs", "ksat", "toeic", "toefl", "teps", "sat", "ielts", "gre", "gmat", "overall"}

func GetQuiz(level int, area string) (*Quiz, error) {
	// We check if the area is valid
	valid := false
	for _, a := range validAreas {
		if a == area {
			valid = true
			break
		}
	}
	if !valid {
		return nil, fmt.Errorf("invalid area")
	}

	if level < 1 || level > 10 {
		return nil, fmt.Errorf("invalid level")
	}

	url := fmt.Sprintf("https://twinword-word-association-quiz.p.rapidapi.com/type1/?level=%d&area=%s", level, area)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
	req.Header.Add("X-RapidAPI-Host", "twinword-word-association-quiz.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var quiz Quiz
	err = json.Unmarshal(body, &quiz)
	if err != nil {
		return nil, err
	}

	return &quiz, nil
}

func TestQuiz() {
	q1, err := GetQuiz(8, "sat")
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", q1)

	q2, err := GetQuiz(5, "toeic")
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", q2)
}

func FakeQuiz(level int, area string) (*Quiz, error) {
	body := []byte(`{"area":"toeic","level":1,"quizlist":[{"quiz":["job","employment","document"],"option":["traffic","application"],"correct":2},{"quiz":["change","file","book"],"option":["building","edit"],"correct":2},{"quiz":["job","professional","adult"],"option":["responsibility","album"],"correct":1},{"quiz":["difficult","hard","bad"],"option":["activity","trying"],"correct":2},{"quiz":["away","by","out"],"option":["outside","group"],"correct":1},{"quiz":["up","front","forward"],"option":["risk","above"],"correct":2},{"quiz":["order","demand","request"],"option":["off","command"],"correct":2},{"quiz":["change","file","book"],"option":["server","edit"],"correct":2},{"quiz":["care","interesting","see"],"option":["attention","girl"],"correct":1},{"quiz":["choice","following","option"],"option":["website","selection"],"correct":2}],"version":"7.5.1","author":"twinword inc.","email":"help@twinword.com","result_code":"200","result_msg":"Success"}`)
	var quiz Quiz
	err := json.Unmarshal(body, &quiz)
	if err != nil {
		return nil, err
	}
	return &quiz, nil
}
