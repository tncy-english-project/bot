package rapidapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
)

type Resp struct {
	TranslatedText string `json:"translatedText"`
}

type TranslateApiResponse struct {
	ResponseData Resp `json:"responseData"`
}

func Translate(text string) (string, error) {

	urlEncode := url.QueryEscape(text)
	url := "https://translated-mymemory---translation-memory.p.rapidapi.com/get?langpair=fr%7Cen&mt=1&onlyprivate=0&de=a%40b.c&q=" + urlEncode

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
	req.Header.Add("X-RapidAPI-Host", "translated-mymemory---translation-memory.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	var apiResponse TranslateApiResponse
	err = json.Unmarshal(body, &apiResponse)
	if err != nil {
		return "", err
	}

	return apiResponse.ResponseData.TranslatedText, nil
}

func FakeTranslate(text string) (string, error) {
	var body = []byte(`{"responseData":{"translatedText":"A complex sentence includes several propositions, including at least one subordinate.","match":0.85},"quotaFinished":false,"mtLangSupported":null,"responseDetails":"","responseStatus":200,"responderId":null,"exception_code":null,"matches":[{"id":0,"segment":"Une phrase complexe comprend plusieurs propositions, dont au moins une subordonn\u00e9e.","translation":"A complex sentence includes several propositions, including at least one subordinate.","source":"fr-FR","target":"en-GB","quality":70,"reference":"Machine Translation.","usage-count":2,"subject":false,"created-by":"MT!","last-updated-by":"MT!","create-date":"2023-03-22 07:57:43","last-update-date":"2023-03-22 07:57:43","match":0.85,"model":"neural"},{"id":"473542809","segment":"Le jeu comprend plusieurs \u00e9tapes, qui comprennent au moins une course. ","translation":"The inventive game is carried out in several stages which comprise at least one race. ","source":"fr-FR","target":"en-GB","quality":"67","reference":null,"usage-count":2,"subject":"Legal_and_Notarial","created-by":"MateCat","last-updated-by":"MateCat","create-date":"2014-12-03 17:57:35","last-update-date":"2014-12-03 17:57:35","match":0.45},{"id":"526190935","segment":"le jeu comprend plusieurs \u00e9tapes, qui comprennent au moins une course","translation":"the inventive game is carried out in several stages which comprise at least one race","source":"fr-FR","target":"en-GB","quality":"67","reference":null,"usage-count":2,"subject":"All","created-by":"MateCat","last-updated-by":"MateCat","create-date":"2011-07-27 18:31:32","last-update-date":"2011-07-27 18:31:32","match":0.44}]}`)

	var apiResponse TranslateApiResponse
	err := json.Unmarshal(body, &apiResponse)
	if err != nil {
		return "", err
	}

	return apiResponse.ResponseData.TranslatedText, nil
}

func TestTranslate() {
	reply, err := Translate("Une phrase complexe comprend plusieurs propositions, dont au moins une subordonnée.")
	if err != nil {
		panic(err)
	}

	fmt.Println(reply)
}
