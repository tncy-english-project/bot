package rapidapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type WordMeaning struct {
	Adjective string `json:"adjective"`
	Adverb    string `json:"adverb"`
	Noun      string `json:"noun"`
	Verb      string `json:"verb"`
}

type DictApiResponse struct {
	Author     string      `json:"author"`
	Email      string      `json:"email"`
	Entry      string      `json:"entry"`
	Ipa        string      `json:"ipa"`
	Meaning    WordMeaning `json:"meaning"`
	Request    string      `json:"request"`
	Response   string      `json:"response"`
	ResultCode string      `json:"result_code"`
	ResultMsg  string      `json:"result_msg"`
	Version    string      `json:"version"`
}

func GetDefinition(word string) (*DictApiResponse, error) {
	url := "https://twinword-word-graph-dictionary.p.rapidapi.com/definition/?entry=" + word

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("X-RapidAPI-Key", "156f073692msh2f418cef42c4035p148b21jsn021a3352cc4b")
	req.Header.Add("X-RapidAPI-Host", "twinword-word-graph-dictionary.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var resp DictApiResponse
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}

	return &resp, nil
}

func TestDict() {
	d, err := GetDefinition("hello")
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", d)
}
