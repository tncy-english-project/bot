package rapidapi

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

func GetRandomWordLength(length int) (string, error) {
	url := "https://random-words5.p.rapidapi.com/getRandom?wordLength=" + fmt.Sprint(length)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
	req.Header.Add("X-RapidAPI-Host", "random-words5.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	word := strings.ToUpper(string(body))

	return word, nil
}
