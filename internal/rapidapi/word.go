package rapidapi

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

func GetRandomWord() (string, error) {
	url := "https://random-words5.p.rapidapi.com/getRandom"

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
	req.Header.Add("X-RapidAPI-Host", "random-words5.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func TestRandomWord() {
	word, err := GetRandomWord()
	if err != nil {
		panic(err)
	}

	fmt.Println(word)
}
