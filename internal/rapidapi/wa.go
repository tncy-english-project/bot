package rapidapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/edgedb/edgedb-go"
	"gitlab.com/tncy-english-project/bot/models"
)

type WaApiResponse struct {
	Entry              string                 `json:"entry"`
	Request            string                 `json:"request"`
	Response           map[string]interface{} `json:"response"`
	Associations       string                 `json:"associations"`
	AssociationsArr    []string               `json:"associations_array"`
	AssociationsScores map[string]float64     `json:"associations_scored"`
	Version            string                 `json:"version"`
	Author             string                 `json:"author"`
	Email              string                 `json:"email"`
	ResultCode         string                 `json:"result_code"`
	ResultMsg          string                 `json:"result_msg"`
}

func GetWA() (*models.WordAssociation, error) {
	// We find a random word
	word, err := GetRandomWord()
	if err != nil {
		return nil, err
	}

	// We create a new game
	var wa = &models.WordAssociation{
		Word: word,
	}

	// We fill it using the API
	url := "https://twinword-word-associations-v1.p.rapidapi.com/associations/"
	payload := strings.NewReader("entry=" + word)
	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
	req.Header.Add("X-RapidAPI-Host", "twinword-word-associations-v1.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)

	var apiResponse WaApiResponse
	err = json.Unmarshal(body, &apiResponse)
	if err != nil {
		return nil, err
	}

	for _, v := range apiResponse.AssociationsArr {
		wa.CorrectAnswers = append(wa.CorrectAnswers, models.WordAssociationAnswer{
			Word:      v,
			Proximity: edgedb.NewOptionalFloat64(apiResponse.AssociationsScores[v]),
		})
	}

	return wa, nil
}

func FakeWA() (*models.WordAssociation, error) {
	var body = []byte(`{"entry":"brother","request":"brother","response":{"brother":1},"associations":"uncle, sister, grandparent, cousin, aunt, elder, sisterhood, mother, sibling, family, parent, familial, maternal, buddy, father, avuncular, relationship, comrade, crony, twin, paternal, niece, kinsfolk, daughter, fraternal, grandma, nephew, friend, partner, mate","associations_array":["uncle","sister","grandparent","cousin","aunt","elder","sisterhood","mother","sibling","family","parent","familial","maternal","buddy","father","avuncular","relationship","comrade","crony","twin","paternal","niece","kinsfolk","daughter","fraternal","grandma","nephew","friend","partner","mate"],"associations_scored":{"uncle":0.45879101007191325,"sister":0.4130010255635193,"grandparent":0.18190730942362335,"cousin":0.44596200353388243,"aunt":0.3351682351181759,"elder":0.24943838379281746,"sisterhood":0.14611188368959965,"mother":0.27871188638430133,"sibling":0.36137138043066175,"family":0.5417825538755892,"parent":0.29765674989964064,"familial":0.22670515456693816,"maternal":0.11846211299961394,"buddy":0.28253358993164224,"father":0.15767882836117308,"avuncular":0.1437678305652311,"relationship":0.29867545131141304,"comrade":0.2616970360776392,"crony":0.2364704693339405,"twin":0.22444763444335258,"paternal":0.1366490327086045,"niece":0.12072716839377161,"kinsfolk":0.12765936526852265,"daughter":0.10352348067916525,"fraternal":0.20819926873663622,"grandma":0.0514733234013734,"nephew":0.14573176137900537,"friend":0.5319181023775056,"partner":0.16338726406463802,"mate":0.20207803483280054},"version":"7.5.1","author":"twinword inc.","email":"help@twinword.com","result_code":"200","result_msg":"Success"}`)

	var apiResponse WaApiResponse
	err := json.Unmarshal(body, &apiResponse)
	if err != nil {
		return nil, err
	}

	var wa = &models.WordAssociation{
		Word: "brother",
	}

	for _, v := range apiResponse.AssociationsArr {
		wa.CorrectAnswers = append(wa.CorrectAnswers, models.WordAssociationAnswer{
			Word:      v,
			Proximity: edgedb.NewOptionalFloat64(apiResponse.AssociationsScores[v]),
		})
	}

	return wa, nil
}

func TestWA() {
	wa, err := GetWA()
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v\n", wa)
}
