package rapidapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

type GptApiResponse struct {
	ConversationID string `json:"conversationId"`
	Reply          string `json:"response"`
}

func AskGPT(text, conversation string) (*GptApiResponse, error) {
	url := "https://chatgpt-ai-chat-bot.p.rapidapi.com/ask"

	payload := strings.NewReader("{\"query\": \"" + text + "\",\"conversationId\":\"" + conversation + "\"}")
	if conversation == "" {
		payload = strings.NewReader("{\"query\": \"" + text + "\"}")
	}

	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	req.Header.Add("content-type", "application/json")
	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
	req.Header.Add("X-RapidAPI-Host", "chatgpt-ai-chat-bot.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var apiResponse GptApiResponse
	err = json.Unmarshal(body, &apiResponse)
	if err != nil {
		return nil, err
	}

	return &apiResponse, nil
}

func FakeGPT(text string, conversation string) (*GptApiResponse, error) {
	var body = []byte(`{"conversationId":"95a4f9e4-deef-4252-bcc6-8f08a387fb9c","response":"Hi, how can I help you?"}`)

	var apiResponse GptApiResponse
	err := json.Unmarshal(body, &apiResponse)
	if err != nil {
		return nil, err
	}

	return &apiResponse, nil
}

func TestGPT() {
	reply, err := AskGPT("Hello", "")
	if err != nil {
		panic(err)
	}

	fmt.Println(reply)
}
