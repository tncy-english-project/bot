package rapidapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

type CompleteResponse struct {
	ID      string `json:"id"`
	Status  string `json:"status"`
	URL     string `json:"url"`
	JobTime int    `json:"job_time"`
}

type CreateJobRespones struct {
	JobID  string `json:"id"`
	Status string `json:"status"`
	ETA    int    `json:"eta"`
	Text   string `json:"text"`
}

func TTS(text string) ([]byte, error) {

	url := "https://large-text-to-speech.p.rapidapi.com/tts"

	payload := strings.NewReader("{\"text\": \"" + text + "\"}")

	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		return nil, err
	}

	req.Header.Add("content-type", "application/json")
	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
	req.Header.Add("X-RapidAPI-Host", "large-text-to-speech.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var apiResponse CreateJobRespones
	err = json.Unmarshal(body, &apiResponse)
	if err != nil {
		return nil, err
	}

	time.Sleep(1 * time.Second)
	var dlURL string

	for {
		url = "https://large-text-to-speech.p.rapidapi.com/tts?id=" + apiResponse.JobID
		req, err = http.NewRequest("GET", url, nil)
		if err != nil {
			time.Sleep(1 * time.Second)
			continue
		}

		req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
		req.Header.Add("X-RapidAPI-Host", "large-text-to-speech.p.rapidapi.com")

		res, err = http.DefaultClient.Do(req)
		if err != nil {
			time.Sleep(1 * time.Second)
			continue
		}

		defer res.Body.Close()
		body, err = io.ReadAll(res.Body)
		if err != nil {
			time.Sleep(1 * time.Second)
			continue
		}

		var apiResponse2 CompleteResponse
		err = json.Unmarshal(body, &apiResponse2)
		if err != nil {
			time.Sleep(1 * time.Second)
			continue
		}

		if apiResponse2.Status == "success" {
			dlURL = apiResponse2.URL
			break
		} else if apiResponse2.Status == "processing" {
			time.Sleep(1 * time.Second)
			continue
		} else {
			return nil, fmt.Errorf("TTS error: %s", apiResponse2.Status)
		}
	}

	// Download the file to a byte array
	resp, err := http.Get(dlURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err = io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func TestTTS() {
	file, err := TTS("Although they fancied squirrels they alternatively enjoyed to wander throughout the forests.")
	if err != nil {
		panic(err)
	}

	err = os.WriteFile("test.wav", file, 0644)
	if err != nil {
		panic(err)
	}
}
