package rapidapi

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
)

type Box struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

type Object struct {
	Label      string  `json:"label"`
	Confidence float64 `json:"confidence"`
	Box        []Box   `json:"bounding-box"`
}

type ObjectApiResponse struct {
	NumberOfObjects int      `json:"number-of-objects"`
	DetectedObjects []Object `json:"detected-objects"`
}

func GetObjects(image string) ([]string, error) {
	urlEncode := url.QueryEscape(image)

	url := "https://mantis-object-detection.p.rapidapi.com/rest/v1/public/detectObjects/json?url=" + urlEncode

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPID_API"))
	req.Header.Add("X-RapidAPI-Host", "mantis-object-detection.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	var apiResponse ObjectApiResponse
	err = json.Unmarshal(body, &apiResponse)
	if err != nil {
		return nil, err
	}

	var labels []string
	for _, object := range apiResponse.DetectedObjects {
		alreadyIn := false
		for _, label := range labels {
			if label == object.Label {
				alreadyIn = true
			}
		}

		if !alreadyIn {
			labels = append(labels, object.Label)
		}
	}

	return labels, nil
}

func FakeObjectDetect(url string) ([]string, error) {
	var body = []byte(`{"number-of-objects":5,"detected-objects":[{"label":"chair","confidence":99.91797804832458,"bounding-box":[{"x":0.2993265986442566,"y":0.5806843042373657},{"x":0.4645167589187622,"y":0.5806843042373657},{"x":0.4645167589187622,"y":0.8818614482879639},{"x":0.2993265986442566,"y":0.8818614482879639}]},{"label":"diningtable","confidence":97.90835976600647,"bounding-box":[{"x":0.27733081579208374,"y":0.5643614530563354},{"x":0.6935773491859436,"y":0.5643614530563354},{"x":0.6935773491859436,"y":0.8967283964157104},{"x":0.27733081579208374,"y":0.8967283964157104}]},{"label":"chair","confidence":96.45293354988098,"bounding-box":[{"x":0.2333013266324997,"y":0.5552465915679932},{"x":0.33422496914863586,"y":0.5552465915679932},{"x":0.33422496914863586,"y":0.8267747163772583},{"x":0.2333013266324997,"y":0.8267747163772583}]}]}`)

	var apiResponse ObjectApiResponse
	err := json.Unmarshal(body, &apiResponse)
	if err != nil {
		return nil, err
	}

	var labels []string
	for _, object := range apiResponse.DetectedObjects {
		alreadyIn := false
		for _, label := range labels {
			if label == object.Label {
				alreadyIn = true
			}
		}

		if !alreadyIn {
			labels = append(labels, object.Label)
		}
	}

	return labels, nil
}

func TestObjectDetect() {
	labels, err := GetObjects("https://www.crack.be/PIMStorage/MainImage/Salle-a-manger-Nelson-st1804-decor-french-oak-ambi-new-Bauwens-GBO.jpg?mode=max&width=1200")
	if err != nil {
		panic(err)
	}

	fmt.Println(labels)
}
