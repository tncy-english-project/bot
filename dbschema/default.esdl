module default {
  type User {
    required property discord_id -> str {
      constraint exclusive;
    }
    property wa_points -> int64;
    property wa_games_played -> int64;
    property quiz_points -> int64;
    property quiz_games_played -> int64;
    property wordle_points -> int64;
    property wordle_games_played -> int64;
    property lyrics_points -> int64;
  }
};