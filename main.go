package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/joho/godotenv"
	"gitlab.com/tncy-english-project/bot/discord"
)

func main() {
	godotenv.Load()

	// rapidapi.TestDict()
	// rapidapi.TestRandomWord()
	// rapidapi.TestQuiz()
	// rapidapi.TestTTS()
	// rapidapi.TestTranslate()
	// rapidapi.TestWA()
	// rapidapi.TestGPT()
	// rapidapi.TestObjectDetect()
	// return

	StartDiscord()

	// Wait here until CTRL-C or other term signal is received.
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc

	discord.Session.Close()
}
