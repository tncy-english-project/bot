package cmd_system

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
)

var PingComand = &discord.Command{
	Name:        "ping",
	Description: "Le bot va répondre pong.",
	Args: []*discord.Arg{
		{
			Name:        "rep",
			Description: "Ce que doit répondre le bot.",
			Required:    false,
			Type:        discordgo.ApplicationCommandOptionString,
		},
	},
	Call: ping,
}

func ping(ctx *discord.CmdContext) {
	rep := ctx.Arguments.GetArg("rep", 0, "pong").Value.(string)
	ctx.Reply(discord.ReplyParams{
		Content: rep,
	})
}
