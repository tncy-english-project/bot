package cmd_game

import (
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"

	"gitlab.com/tncy-english-project/bot/discord"
	"gitlab.com/tncy-english-project/bot/internal/rapidapi"
)

var QuizCommand = &discord.Command{
	Name:        "quiz",
	Description: "Le bot va commencer un quiz",
	Args: []*discord.Arg{
		{
			Name:        "level",
			Description: "Le niveau de difficulté.",
			Required:    false,
			Type:        discordgo.ApplicationCommandOptionInteger,
			Choices: []*discord.Choice{
				{
					Name:  "1",
					Value: 1,
				},
				{
					Name:  "2",
					Value: 2,
				},
				{
					Name:  "3",
					Value: 3,
				},
				{
					Name:  "4",
					Value: 4,
				},
				{
					Name:  "5",
					Value: 5,
				},
				{
					Name:  "6",
					Value: 6,
				},
				{
					Name:  "7",
					Value: 7,
				},
				{
					Name:  "8",
					Value: 8,
				},
				{
					Name:  "9",
					Value: 9,
				},
				{
					Name:  "10",
					Value: 10,
				},
			},
		},
		{
			Name:        "type",
			Description: "Le type de quiz.",
			Required:    false,
			Type:        discordgo.ApplicationCommandOptionString,
			Choices: []*discord.Choice{
				{
					Name:  "toeic",
					Value: "toeic",
				},
				{
					Name:  "toefl",
					Value: "toefl",
				},
				{
					Name:  "ielts",
					Value: "ielts",
				},
				{
					Name:  "gre",
					Value: "gre",
				},
				{
					Name:  "sat",
					Value: "sat",
				},
				{
					Name:  "gmat",
					Value: "gmat",
				},
				{
					Name:  "ksat",
					Value: "ksat",
				},
				{
					Name:  "es",
					Value: "es",
				},
				{
					Name:  "ms",
					Value: "ms",
				},
				{
					Name:  "hs",
					Value: "hs",
				},
				{
					Name:  "teps",
					Value: "teps",
				},
				{
					Name:  "overall",
					Value: "overall",
				},
			},
		},
	},
	Call: startQuiz,
}

type QuizData struct {
	Game    *rapidapi.Quiz
	Message *discordgo.Message
	Step    int
}

func startQuiz(ctx *discord.CmdContext) {
	// We reply to discord saying that we are processing the request
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
		},
	})

	level := ctx.Arguments.GetArg("level", 0, 3.0).Value.(float64)
	area := ctx.Arguments.GetArg("type", 1, "toeic").Value.(string)

	// We get a new game
	quiz, err := rapidapi.GetQuiz(int(level), area)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content: "An error occured while getting a new game : " + err.Error(),
		})
		return
	}

	var buttons []discordgo.MessageComponent

	for i, option := range quiz.QuizList[0].Option {
		buttons = append(buttons, discordgo.Button{
			Label:    option,
			Style:    discordgo.SuccessButton,
			CustomID: ctx.ID + "-" + strconv.Itoa(i),
		})
	}

	var data = &QuizData{
		Game: quiz,
		Step: 1,
		Message: &discordgo.Message{
			Embeds: []*discordgo.MessageEmbed{
				{

					Title:       "Quiz: " + quiz.Area + " Level: " + strconv.Itoa(quiz.Level) + " Step: 1/10",
					Description: "Find the closest match to the question.",
					Color:       0x00ff00,
					Fields: []*discordgo.MessageEmbedField{
						{
							Name:   "Answers",
							Value:  "",
							Inline: false,
						},
						{
							Name:   "Question",
							Value:  quiz.QuizList[0].Quiz[0] + " " + quiz.QuizList[0].Quiz[1] + " " + quiz.QuizList[0].Quiz[2],
							Inline: false,
						},
					},
				},
			},
			Components: []discordgo.MessageComponent{
				discordgo.ActionsRow{
					Components: buttons,
				},
			},
		},
	}
	ctx.Reply(discord.ReplyParams{
		Content:    data.Message.Embeds[0],
		Components: data.Message.Components,
		FollowUp:   true,
	})

	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Data:          data,
		Call:          handleQuizChoice,
	}, 0)
}

func handleQuizChoice(ctx *discord.CmdContext) {
	if ctx.Author.ID != ctx.Menu.SourceContext.Author.ID {
		ctx.Reply(discord.ReplyParams{
			Content:   "You are not the author of this command",
			Ephemeral: true,
		})
		return
	}
	split := strings.Split(ctx.ComponentData.CustomID, "-")
	choice := split[1]
	a, err := strconv.Atoi(choice)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content:   "An error occured while parsing the choice : " + err.Error(),
			Ephemeral: true,
		})
		return
	}
	var data = ctx.Menu.Data.(*QuizData)

	data.Message.Embeds[0].Title = "Quiz: " + data.Game.Area + " Level: " + strconv.Itoa(data.Game.Level) + " Step: " + strconv.Itoa(data.Step+1) + "/" + strconv.Itoa(len(data.Game.QuizList))
	if a == data.Game.QuizList[data.Step-1].Correct-1 {
		data.Message.Embeds[0].Fields[0].Value += "✅"
	} else {
		data.Message.Embeds[0].Fields[0].Value += "❌"
	}
	var buttons []discordgo.MessageComponent

	if data.Step == len(data.Game.QuizList) {
		endQuiz(ctx)
		return
	}

	data.Message.Embeds[0].Fields[1].Value = data.Game.QuizList[data.Step].Quiz[0] + " " + data.Game.QuizList[data.Step].Quiz[1] + " " + data.Game.QuizList[data.Step].Quiz[2]
	for i, option := range data.Game.QuizList[data.Step].Option {
		buttons = append(buttons, discordgo.Button{
			Label:    option,
			Style:    discordgo.SuccessButton,
			CustomID: ctx.Menu.SourceContext.ID + "-" + strconv.Itoa(i),
		})
	}
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredMessageUpdate,
		},
	})
	_, err = ctx.Reply(discord.ReplyParams{
		Content: data.Message.Embeds[0],
		Components: []discordgo.MessageComponent{
			discordgo.ActionsRow{
				Components: buttons,
			},
		},
		Edit: true,
	})
	data.Step++
}

func endQuiz(ctx *discord.CmdContext) {
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredMessageUpdate,
		},
	})

	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Call:          startQuiz,
	}, 0)

	var response = ctx.Menu.Data.(*QuizData).Message.Embeds[0].Fields[0].Value
	// compute score here ✅ +10 ❌ -5

	var score = 0
	for _, r := range response {
		if r == '✅' {
			score += 10
		} else {
			score -= 5
		}
	}
	if score < 0 {
		score = 0
	}

	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.MessageEmbed{
			Title:       "Quiz finished",
			Description: "You can start a new game",
			Color:       0x00ff00,
			Fields: []*discordgo.MessageEmbedField{
				{
					Name:  "Answers",
					Value: ctx.Menu.Data.(*QuizData).Message.Embeds[0].Fields[0].Value,
				},
				{
					Name:  "Score",
					Value: "Here are your points: **" + strconv.Itoa(score) + "**",
				},
			},
		},
		Components: []discordgo.MessageComponent{
			discordgo.ActionsRow{
				Components: []discordgo.MessageComponent{
					discordgo.Button{
						Label:    "New game",
						Style:    discordgo.PrimaryButton,
						CustomID: ctx.ID,
					},
				},
			},
		},
		Edit: true,
	})
	// transform score in int64
	score64 := int64(score)
	ctx.User.AddQuizPoints(score64)
	ctx.User.AddQuizPlayed(1)

}
