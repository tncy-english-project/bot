package cmd_game

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
	"gitlab.com/tncy-english-project/bot/internal/rapidapi"
)

var TranslateCommand = &discord.Command{
	Name:        "translate",
	Description: "Le bot va traduire.",
	Args: []*discord.Arg{
		{
			Name:        "text",
			Description: "What do you want me to translate ?",
			Required:    true,
			Type:        discordgo.ApplicationCommandOptionString,
		},
	},
	Call: translate,
}

var TranslateMessageCommand = &discord.Command{
	Type: discordgo.MessageApplicationCommand,
	Name: "translate",
	Call: translate,
}

func translate(ctx *discord.CmdContext) {
	// We reply to discord saying that we are processing the request
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
		},
	})

	var text string

	// Check if the command comes from a message
	msgID := ctx.Interaction.ApplicationCommandData().TargetID

	if msgID == "" {
		// We get the text
		text = ctx.Arguments.GetArg("text", 100, "Hello World !").Value.(string)
	} else {
		msg, err := ctx.Session.ChannelMessage(ctx.ChannelID, msgID)
		if err != nil {
			ctx.Reply(discord.ReplyParams{
				Content:  "An error occured while processing your request :" + err.Error(),
				FollowUp: true,
			})
			return
		}

		text = msg.Content
	}

	// We translate the text
	translatedText, err := rapidapi.Translate(text)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content:  "An error occured while processing your request :" + err.Error(),
			FollowUp: true,
		})
		return
	}

	// We create the menu
	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Data:          translatedText,
		Call:          translateTTS,
	}, 0)

	// We reply to discord with the translated text
	ctx.Reply(discord.ReplyParams{
		Content: "Here you go : `" + translatedText + "`",
		Components: []discordgo.MessageComponent{
			discordgo.ActionsRow{
				Components: []discordgo.MessageComponent{
					discordgo.Button{
						Label:    "Pronunciation",
						Style:    discordgo.SuccessButton,
						CustomID: ctx.ID,
					},
				},
			},
		},
		FollowUp: true,
	})
}

func translateTTS(ctx *discord.CmdContext) {
	// We get the text
	text := ctx.Menu.Data.(string)

	// Send this to the TTS command
	ctx.Arguments = []*discord.CommandArg{
		{
			Name:  "text",
			Value: "Here is how I would pronounce it : " + text,
			Found: true,
		},
	}

	// We call the TTS command
	TTSCommand.Call(ctx)
}
