package cmd_game

import (
	"bytes"
	"os"
	"strings"
	"time"

	"github.com/bwmarrin/dgvoice"
	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
	"gitlab.com/tncy-english-project/bot/internal/rapidapi"
)

var TTSCommand = &discord.Command{
	Name:        "talk",
	Description: "Le bot va répondre pong.",
	Args: []*discord.Arg{
		{
			Name:        "text",
			Description: "What do you want me to say ?",
			Required:    true,
			Type:        discordgo.ApplicationCommandOptionString,
		},
	},
	Call: tts,
}

var TTSMessageCommand = &discord.Command{
	Type: discordgo.MessageApplicationCommand,
	Name: "tts",
	Call: tts,
}

func tts(ctx *discord.CmdContext) {
	// We reply to discord saying that we are processing the request
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
		},
	})

	var text string

	// Check if the command comes from a message
	var msgID string
	if ctx.Interaction.Type == discordgo.InteractionApplicationCommand {
		msgID = ctx.Interaction.ApplicationCommandData().TargetID
	}

	if msgID == "" {
		// We get the text
		text = ctx.Arguments.GetArg("text", 100, "Hello World !").Value.(string)
	} else {
		msg, err := ctx.Session.ChannelMessage(ctx.ChannelID, msgID)
		if err != nil {
			ctx.Reply(discord.ReplyParams{
				Content:  "An error occured while processing your request :" + err.Error(),
				FollowUp: true,
			})
			return
		}

		text = msg.Content
	}

	if strings.Contains(text, "shawty") || strings.Contains(text, "Shawty") {
		// We open the file test.mp3
		file, err := os.ReadFile("test.mp3")
		if err != nil {
			if err != nil {
				ctx.Reply(discord.ReplyParams{
					Content:  "An error occured while processing your request :" + err.Error(),
					FollowUp: true,
				})
				return
			}
		}

		// We create the buffer
		buf := bytes.NewBuffer(file)

		time.Sleep(5 * time.Second)

		// We check if the user is in a voice channel
		vc, err := ctx.Session.State.VoiceState(ctx.GuildID, ctx.Author.ID)
		if err == nil {
			ctx.Reply(discord.ReplyParams{
				Content: &discordgo.MessageSend{
					Content: "Here we go!",
					Files: []*discordgo.File{
						{
							Name:   "tts.wav",
							Reader: buf,
						},
					},
				},
				FollowUp: true,
			})

			// We join the voice channel
			vc, err := ctx.Session.ChannelVoiceJoin(ctx.GuildID, vc.ChannelID, false, true)
			if err != nil {
				ctx.Reply(discord.ReplyParams{
					Content:  "An error occured while processing your request :" + err.Error(),
					FollowUp: true,
				})
				return
			}

			dgvoice.PlayAudioFile(vc, "test.mp3", make(chan bool))
			return
		}

		// We send the file
		ctx.Reply(discord.ReplyParams{
			Content: &discordgo.MessageSend{
				Content: "Here is your file :",
				Files: []*discordgo.File{
					{
						Name:   "tts.wav",
						Reader: buf,
					},
				},
			},
			FollowUp: true,
		})
		return
	}

	// We send the text
	file, err := rapidapi.TTS(text)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content:  "An error occured while processing your request :" + err.Error(),
			FollowUp: true,
		})
		return
	}

	// We create the buffer
	buf := bytes.NewBuffer(file)

	// We check if the user is in a voice channel
	vc, err := ctx.Session.State.VoiceState(ctx.GuildID, ctx.Author.ID)
	if err == nil {
		ctx.Reply(discord.ReplyParams{
			Content: &discordgo.MessageSend{
				Content: "Here we go! Here's the file if you want to replay.",
				Files: []*discordgo.File{
					{
						Name:   "tts.wav",
						Reader: buf,
					},
				},
			},
			FollowUp: true,
		})

		// We save in temp.wav
		err = os.WriteFile("temp.wav", file, 0644)
		if err != nil {
			ctx.Reply(discord.ReplyParams{
				Content:  "An error occured while processing your request :" + err.Error(),
				FollowUp: true,
			})
			return
		}

		// We join the voice channel
		vc, err := ctx.Session.ChannelVoiceJoin(ctx.GuildID, vc.ChannelID, false, true)
		if err != nil {
			ctx.Reply(discord.ReplyParams{
				Content:  "An error occured while processing your request :" + err.Error(),
				FollowUp: true,
			})
			return
		}

		// We play the file in the voice channel
		dgvoice.PlayAudioFile(vc, "temp.wav", make(chan bool))
		return
	}

	// We send the file
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.MessageSend{
			Content: "Here is your file :",
			Files: []*discordgo.File{
				{
					Name:   "tts.wav",
					Reader: buf,
				},
			},
		},
		FollowUp: true,
	})
}
