package cmd_game

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
	"gitlab.com/tncy-english-project/bot/internal/rapidapi"
)

var LyricsCommand = &discord.Command{
	Name:        "lyrics",
	Description: "Jouer au jeu des lyrics.",
	Call:        lyrics,
}

type Track struct {
	TrackID string `json:"track_id"`
	Track   string `json:"track_name"`
	Artist  string `json:"artist_name"`
}

var tracks = []Track{
	{
		TrackID: "9348147",
		Track:   "Replay",
		Artist:  "Iyaz",
	},
	{
		TrackID: "253116049",
		Track:   "I Can Buy Myself Flowers",
		Artist:  "Miley Cyrus",
	},
	{
		TrackID: "226270777",
		Track:   "Enemy",
		Artist:  "Imagine Dragons",
	},
}

var at = 0

func lyrics(ctx *discord.CmdContext) {
	// We reply to discord saying that we are processing the request
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
		},
	})

	// We get the next track
	track := tracks[at]
	at++
	if at >= len(tracks) {
		at = 0
	}
	trackID := track.TrackID

	// We get the lyrics
	lyrics, err := rapidapi.GetLyrics(trackID)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content:  "An error occured while processing your request :" + err.Error(),
			FollowUp: true,
		})
		return
	}

	// We translate the lyrics
	translation, err := rapidapi.TranslateLyrics(lyrics)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content:  "An error occured while processing your request :" + err.Error(),
			FollowUp: true,
		})
		return
	}

	translation.Data.TranslatedText = strings.ReplaceAll(translation.Data.TranslatedText, "\\ n", "\n")

	// Truncate at 1024 characters
	if len(translation.Data.TranslatedText) > 500 {
		translation.Data.TranslatedText = translation.Data.TranslatedText[:500]
	}

	// We create the embed
	embed := &discordgo.MessageEmbed{
		Title:       "Guess the song",
		Description: "This song has been translated into French. Can you guess the original song's name?",
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:  "Lyrics",
				Value: translation.Data.TranslatedText,
			},
		},
	}

	// We create the menu
	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Data:          track,
		Call:          lyricsButton,
	}, 0)

	// We send the embed
	ctx.Reply(discord.ReplyParams{
		Content: embed,
		Components: []discordgo.MessageComponent{
			discordgo.ActionsRow{
				Components: []discordgo.MessageComponent{
					discordgo.Button{
						Label:    "Answer",
						Style:    discordgo.PrimaryButton,
						CustomID: ctx.ID,
					},
				},
			},
		},
		FollowUp: true,
	})
}

func lyricsButton(ctx *discord.CmdContext) {
	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Data:          ctx.Menu.Data,
		Call:          lyricsAnswer,
	}, 0)

	// Send the modal
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseModal,
			Data: &discordgo.InteractionResponseData{
				CustomID: ctx.ID,
				Title:    "What's your anwser ?",
				Components: []discordgo.MessageComponent{
					discordgo.ActionsRow{
						Components: []discordgo.MessageComponent{
							discordgo.TextInput{
								Label:       "Answer",
								Placeholder: "Your answer",
								CustomID:    "answer",
								Required:    true,
								Style:       discordgo.TextInputShort,
							},
						},
					},
				},
			},
		},
	})
}

func lyricsAnswer(ctx *discord.CmdContext) {
	// Get the answer from the modal
	var answer = ctx.ModalData.Components[0].(*discordgo.ActionsRow).Components[0].(*discordgo.TextInput).Value

	// We get the data
	track := ctx.Menu.Data.(Track)

	// We send a message to the user to tell him that the bot is thinking
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
		},
	})

	// We add the point to the user
	ctx.User.AddLyricsPoints(1)

	// We verify that it atleast one contains the other
	if strings.Contains(strings.ToLower(answer), strings.ToLower(track.Track)) || strings.Contains(strings.ToLower(track.Track), strings.ToLower(answer)) {
		ctx.Reply(discord.ReplyParams{
			Content:  "You're right, you earned 1 point !",
			FollowUp: true,
		})
	} else {
		ctx.Reply(discord.ReplyParams{
			Content:  fmt.Sprintf("You're wrong, the answer was `%s` by `%s`", track.Track, track.Artist),
			FollowUp: true,
		})
	}
}
