package cmd_game

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
	"gitlab.com/tncy-english-project/bot/internal/rapidapi"
	"gitlab.com/tncy-english-project/bot/models"
)

var WAComand = &discord.Command{
	Name:        "wa",
	Description: "Le bot va commencer la partie.",
	Call:        startWA,
}

type WAData struct {
	Game    *models.WordAssociation
	Message *discordgo.Message
}

func startWA(ctx *discord.CmdContext) {
	// We reply to discord saying that we are processing the request
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
		},
	})
	// We get a new game
	wa, err := rapidapi.GetWA()
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content: "An error occured while getting a new game : " + err.Error(),
		})
		return
	}

	// We create the data linked to the game
	var data = &WAData{
		Game: wa,
		Message: &discordgo.Message{
			Embeds: []*discordgo.MessageEmbed{
				{

					Title:       "Word Association",
					Description: "You have **" + strconv.Itoa(len(wa.CorrectAnswers)) + "** tries to find the most words associated to the word : `" + wa.Word + "`\n",
					Color:       0x00ff00,
				},
			},
			Components: []discordgo.MessageComponent{
				discordgo.ActionsRow{
					Components: []discordgo.MessageComponent{
						discordgo.Button{
							Label:    "Submit an answer",
							Style:    discordgo.SuccessButton,
							CustomID: ctx.ID + "-submit",
						},
						discordgo.Button{
							Label:    "Give up",
							Style:    discordgo.DangerButton,
							CustomID: ctx.ID + "-giveup",
						},
					},
				},
			},
		},
	}
	ctx.Reply(discord.ReplyParams{
		Content:    data.Message.Embeds[0],
		Components: data.Message.Components,
		FollowUp:   true,
	})

	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Data:          data,
		Call:          handleChoice,
	}, 0)
}

func handleChoice(ctx *discord.CmdContext) {
	if ctx.Author.ID != ctx.Menu.SourceContext.Author.ID {
		ctx.Reply(discord.ReplyParams{
			Content: "You are not the author of this command",
		})
		return
	}

	split := strings.Split(ctx.ComponentData.CustomID, "-")
	choice := split[1]

	switch choice {
	case "submit":
		sendModal(ctx)
	case "giveup":
		endWa(ctx)
	}

}

func sendModal(ctx *discord.CmdContext) {
	if ctx.Menu.Data.(*WAData).Game.Finished {
		ctx.Reply(discord.ReplyParams{
			Content:   "The game is already finished",
			Ephemeral: true,
		})
		return
	}

	// We update the data
	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx.Menu.SourceContext,
		Data:          ctx.Menu.Data,
		Call:          answerWA,
	}, 0)

	// Send the modal
	_, err := ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseModal,
			Data: &discordgo.InteractionResponseData{
				CustomID: ctx.ID,
				Title:    "Find the words associated to : " + ctx.Menu.Data.(*WAData).Game.Word,
				Components: []discordgo.MessageComponent{
					discordgo.ActionsRow{
						Components: []discordgo.MessageComponent{
							discordgo.TextInput{
								Label:       "Answer",
								Placeholder: "Your answer",
								CustomID:    "answer",
								Required:    true,
								Style:       discordgo.TextInputShort,
							},
						},
					},
				},
			},
		},
	})

	if err != nil {
		fmt.Println(err)
	}
}

func answerWA(ctx *discord.CmdContext) {
	if ctx.Author.ID != ctx.Menu.SourceContext.Author.ID {
		ctx.Reply(discord.ReplyParams{
			Content:  "You are not the author of this command",
			FollowUp: true,
		})
		return
	}

	// We get the answer
	var answer = ctx.ModalData.Components[0].(*discordgo.ActionsRow).Components[0].(*discordgo.TextInput).Value

	// Check if the answer is correct
	var correct = false
	var points = 0
	for _, v := range ctx.Menu.Data.(*WAData).Game.CorrectAnswers {
		if v.Word == answer {
			correct = true
			p, _ := v.Proximity.Get()
			points = int(100.0 * p)
			break
		}
	}

	// Check if the answer has already been given
	for _, v := range ctx.Menu.Data.(*WAData).Game.ProvidedAnswers {
		if v.Word == answer {
			ctx.Reply(discord.ReplyParams{
				Content:   "You already gave this answer",
				Ephemeral: true,
				FollowUp:  true,
			})
			return
		}
	}

	// We add the answer to the list of provided answers
	ctx.Menu.Data.(*WAData).Game.ProvidedAnswers = append(ctx.Menu.Data.(*WAData).Game.ProvidedAnswers, models.WordAssociationAnswer{
		Word: answer,
	})

	// We send the answer
	if correct {
		ctx.Menu.Data.(*WAData).Message.Embeds[0].Description += fmt.Sprintf("\n%s: `✅` (+%d points)", answer, points)
	} else {
		ctx.Menu.Data.(*WAData).Message.Embeds[0].Description += "\n" + answer + ": `❌`"
	}
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseUpdateMessage,
		},
	})
	ctx.Reply(discord.ReplyParams{
		Content:    ctx.Menu.Data.(*WAData).Message.Embeds[0],
		Components: ctx.Menu.Data.(*WAData).Message.Components,
		ID:         ctx.Menu.Data.(*WAData).Message.ID,
		Edit:       true,
	})

	// We check if the game is over
	if len(ctx.Menu.Data.(*WAData).Game.ProvidedAnswers) == len(ctx.Menu.Data.(*WAData).Game.CorrectAnswers) {
		endWa(ctx)
	}
}

func endWa(ctx *discord.CmdContext) {
	if ctx.Menu.Data.(*WAData).Game.Finished {
		ctx.Reply(discord.ReplyParams{
			Content:   "The game is already finished",
			Ephemeral: true,
		})
		return
	}
	discord.ActiveMenus.Delete(ctx.ID)

	// Change the original message to show the result
	ctx.Menu.Data.(*WAData).Message.Embeds[0].Description += "\n\nThe game is over, the correct answers are the following :"

	answers := ""
	for _, v := range ctx.Menu.Data.(*WAData).Game.CorrectAnswers {
		answers += v.Word + ", "
	}
	answers = answers[:len(answers)-2]
	ctx.Menu.Data.(*WAData).Message.Embeds[0].Description += "\n" + answers

	// Add the number of points (number of given answers that are correct)
	var points int64
	for _, v := range ctx.Menu.Data.(*WAData).Game.CorrectAnswers {
		for _, v2 := range ctx.Menu.Data.(*WAData).Game.ProvidedAnswers {
			if v.Word == v2.Word {
				p, _ := v.Proximity.Get()
				points += int64(100.0 * p)
				break
			}
		}
	}
	ctx.User.AddWaPlayed(1)
	ctx.User.AddWaPoints(points)

	ctx.Menu.Data.(*WAData).Message.Embeds[0].Description += fmt.Sprintf("\n\nYou got **%d** points!", points)
	ctx.Menu.Data.(*WAData).Game.Finished = true

	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseUpdateMessage,
		},
	})
	ctx.Reply(discord.ReplyParams{
		Content:    ctx.Menu.Data.(*WAData).Message.Embeds[0],
		Components: ctx.Menu.Data.(*WAData).Message.Components,
		ID:         ctx.Menu.Data.(*WAData).Message.ID,
		Edit:       true,
	})
}
