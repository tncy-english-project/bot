package cmd_game

import (
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
	"gitlab.com/tncy-english-project/bot/internal/emojis"
	"gitlab.com/tncy-english-project/bot/internal/rapidapi"
)

var WordleCommand = &discord.Command{
	Name:        "wordle",
	Description: "Le bot va commencer un wordle",
	Args: []*discord.Arg{
		{
			Name:        "level",
			Description: "Le niveau de difficulté.",
			Required:    false,
			Type:        discordgo.ApplicationCommandOptionInteger,
			Choices: []*discord.Choice{
				{
					Name:  "5",
					Value: 5,
				},
				{
					Name:  "6",
					Value: 6,
				},
				{
					Name:  "7",
					Value: 7,
				},
				{
					Name:  "8",
					Value: 8,
				},
				{
					Name:  "9",
					Value: 9,
				},
				{
					Name:  "10",
					Value: 10,
				},
			},
		},
	},
	Call: startWordle,
}

type WordleData struct {
	Word       string
	Guesses    []string
	Embed      *discordgo.MessageEmbed
	Components []discordgo.MessageComponent
}

func (w *WordleData) GetDescription() string {
	// We create the description
	description := "Find the hidden word!\n\n"

	// We add the guesses
	for i := 0; i < len(w.Word); i++ {
		// If there's a guess
		if i < len(w.Guesses) {
			// Draw with colors
			counts := make(map[byte]int)
			for _, guess := range w.Word {
				counts[byte(guess)]++
			}

			for j := 0; j < len(w.Word); j++ {
				if w.Guesses[i][j] == w.Word[j] {
					counts[w.Guesses[i][j]]--
				}
			}

			// Now we draw
			for j := 0; j < len(w.Word); j++ {
				if w.Guesses[i][j] == w.Word[j] {
					// description += "🟩"
					description += emojis.GreenAlphabet[string(w.Guesses[i][j])]
				} else if counts[w.Guesses[i][j]] > 0 {
					description += emojis.YellowAlphabet[string(w.Guesses[i][j])]
					counts[w.Guesses[i][j]]--
				} else {
					e, ok := emojis.GrayAlphabet[string(w.Guesses[i][j])]
					if !ok {
						e = emojis.EmptyLetter
					}
					description += e
				}
			}
		} else {
			for j := 0; j < len(w.Word); j++ {
				description += emojis.EmptyLetter
			}
		}
		description += "\n"
	}

	if len(w.Guesses) > 0 {
		if w.Guesses[len(w.Guesses)-1] == w.Word {
			points := int64(((len(w.Word) - len(w.Guesses)) + 1) * 10)
			description += "\nYou **won**! The word was **" + w.Word + "**."
			description += "\nYou earned **" + strconv.FormatInt(points, 10) + " points**."
		} else if len(w.Guesses) == len(w.Word) {
			description += "\nYou **lost**, the word was **" + w.Word + "**."
		}
	}

	return description
}

func (w *WordleData) GetComponents(ID string) []discordgo.MessageComponent {
	var d = []discordgo.MessageComponent{
		&discordgo.ActionsRow{
			Components: []discordgo.MessageComponent{
				&discordgo.Button{
					Label:    "Answer",
					Style:    discordgo.SuccessButton,
					CustomID: ID + "-answer",
				},
			},
		},
	}

	// If the game is over
	if len(w.Guesses) > 0 && (w.Guesses[len(w.Guesses)-1] == w.Word || len(w.Guesses) == len(w.Word)) {
		d = []discordgo.MessageComponent{
			&discordgo.ActionsRow{
				Components: []discordgo.MessageComponent{
					&discordgo.Button{
						Label:    "New game",
						Style:    discordgo.SuccessButton,
						CustomID: ID + "-new",
					},
					&discordgo.Button{
						Label:    "Pronounce the word",
						Style:    discordgo.SuccessButton,
						CustomID: ID + "-pronounce",
					},
					&discordgo.Button{
						Label:    "Define the word",
						Style:    discordgo.SuccessButton,
						CustomID: ID + "-define",
					},
				},
			},
		}
	}

	return d
}

func startWordle(ctx *discord.CmdContext) {
	// We reply to discord saying that we are processing the request
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
		},
	})

	// We get the text
	level := int(ctx.Arguments.GetArg("level", 0, 5.0).Value.(float64))

	word, err := rapidapi.GetRandomWordLength(level)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content: "Une erreur est survenue lors de la récupération du mot.",
		})
		return
	}

	// We create the game data
	data := &WordleData{
		Word: word,
		Embed: &discordgo.MessageEmbed{
			Title: "Wordle",
			Color: 0x00ff00,
		},
	}
	data.Embed.Description = data.GetDescription()
	data.Components = data.GetComponents(ctx.ID)

	// We create the menu
	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Data:          data,
		Call:          handleWordleButton,
	}, 0)

	// We send the message
	ctx.Reply(discord.ReplyParams{
		Content:    data.Embed,
		Components: data.Components,
		FollowUp:   true,
	})
}

func handleWordleButton(ctx *discord.CmdContext) {
	// We get the data
	data := ctx.Menu.Data.(*WordleData)

	split := strings.Split(ctx.ComponentData.CustomID, "-")
	choice := split[1]

	switch choice {
	case "answer":
		// We update the data
		discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
			MenuID:        ctx.ID,
			SourceContext: ctx.Menu.SourceContext,
			Data:          ctx.Menu.Data,
			Call:          handleWordleAnswer,
		}, 0)

		// Send the modal
		ctx.Reply(discord.ReplyParams{
			Content: &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseModal,
				Data: &discordgo.InteractionResponseData{
					CustomID: ctx.ID,
					Title:    "What do you wish to try ?",
					Components: []discordgo.MessageComponent{
						discordgo.ActionsRow{
							Components: []discordgo.MessageComponent{
								discordgo.TextInput{
									Label:       "Answer",
									Placeholder: "Your answer",
									CustomID:    "answer",
									MinLength:   len(data.Word),
									MaxLength:   len(data.Word),
									Required:    true,
									Style:       discordgo.TextInputShort,
								},
							},
						},
					},
				},
			},
		})
	case "new":
		startWordle(ctx)
	case "pronounce":
		// Send this to the TTS command
		ctx.Arguments = []*discord.CommandArg{
			{
				Name:  "text",
				Value: "Here is how I would pronounce it : " + data.Word,
				Found: true,
			},
		}

		// We call the TTS command
		TTSCommand.Call(ctx)
	case "define":
		// Send this to the Define command
		ctx.Arguments = []*discord.CommandArg{
			{
				Name:  "word",
				Value: data.Word,
				Found: true,
			},
		}

		// We call the Define command
		DefineCommand.Call(ctx)
	}
}

func handleWordleAnswer(ctx *discord.CmdContext) {
	// We get the answer
	var answer = ctx.ModalData.Components[0].(*discordgo.ActionsRow).Components[0].(*discordgo.TextInput).Value
	// We uppercase it
	answer = strings.ToUpper(answer)

	// We get the data
	data := ctx.Menu.Data.(*WordleData)

	// We check if the game is over
	if len(data.Guesses) == len(data.Word) || (len(data.Guesses) > 0 && data.Guesses[len(data.Guesses)-1] == data.Word) {
		return
	}

	// We check if the game is gonna be over
	if answer == data.Word || len(data.Guesses) == len(data.Word)-1 {
		ctx.User.AddWordlePlayed(1)
		if answer == data.Word {
			points := int64(((len(data.Word) - len(data.Guesses)) + 1) * 10)
			ctx.User.AddWordlePoints(points)
		}
	}

	// We update the data
	data.Guesses = append(data.Guesses, answer)
	data.Embed.Description = data.GetDescription()
	data.Components = data.GetComponents(ctx.Menu.SourceContext.ID)

	// We update the message
	ctx.Reply(discord.ReplyParams{
		Content:    data.Embed,
		Components: data.Components,
		Edit:       true,
	})
}
