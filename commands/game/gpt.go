package cmd_game

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
	"gitlab.com/tncy-english-project/bot/internal/rapidapi"
)

var GPTCommand = &discord.Command{
	Name:        "discuss",
	Description: "Le bot va répondre comme un pro.",
	Call:        gptStart,
}

type GptData struct {
	ConversationID string `json:"conversationId"`
	Embeds         []*discordgo.MessageEmbed
	Components     []discordgo.MessageComponent
}

func gptStart(ctx *discord.CmdContext) {
	gptData := &GptData{
		ConversationID: "",
		Embeds: []*discordgo.MessageEmbed{
			{

				Title:       "Chat Box",
				Description: "You can communicate with the bot by clicking on the button below.\n",
				Color:       0x00ff00,
			},
		},
		Components: []discordgo.MessageComponent{
			discordgo.ActionsRow{
				Components: []discordgo.MessageComponent{
					discordgo.Button{
						Label:    "Talk more",
						Style:    discordgo.SuccessButton,
						CustomID: ctx.ID,
					},
				},
			},
		},
	}

	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Data:          gptData,
		Call:          gptTalk,
	}, 0)

	ctx.Reply(discord.ReplyParams{
		Content:    gptData.Embeds[0],
		Components: gptData.Components,
	})
}

func gptTalk(ctx *discord.CmdContext) {
	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Data:          ctx.Menu.Data,
		Call:          gptAnswer,
	}, 0)

	// Send the modal
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseModal,
			Data: &discordgo.InteractionResponseData{
				CustomID: ctx.ID,
				Title:    "What do you wish to ask?",
				Components: []discordgo.MessageComponent{
					discordgo.ActionsRow{
						Components: []discordgo.MessageComponent{
							discordgo.TextInput{
								Label:       "Text",
								Placeholder: "Your question",
								CustomID:    "text",
								Required:    true,
								Style:       discordgo.TextInputShort,
							},
						},
					},
				},
			},
		},
	})
}

func gptAnswer(ctx *discord.CmdContext) {
	// Get the text from the modal
	var text = ctx.ModalData.Components[0].(*discordgo.ActionsRow).Components[0].(*discordgo.TextInput).Value

	// We send a message to the user to tell him that the bot is thinking
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseUpdateMessage,
		},
	})

	// We update the embed
	ctx.Menu.Data.(*GptData).Embeds[0].Description = "The bot is thinking..."

	ctx.Reply(discord.ReplyParams{
		Content:    ctx.Menu.Data.(*GptData).Embeds[0],
		Components: ctx.Menu.Data.(*GptData).Components,
		Edit:       true,
	})

	// We send that to the API
	out, err := rapidapi.AskGPT(text, ctx.Menu.Data.(*GptData).ConversationID)
	if err != nil {
		ctx.Menu.Data.(*GptData).Embeds[0].Description = "An error occurred while communicating with the bot."
		ctx.Reply(discord.ReplyParams{
			Content:    ctx.Menu.Data.(*GptData).Embeds[0],
			Components: ctx.Menu.Data.(*GptData).Components,
			Edit:       true,
		})
		return
	}

	// We update the conversation ID
	ctx.Menu.Data.(*GptData).ConversationID = out.ConversationID

	// We update the embed
	ctx.Menu.Data.(*GptData).Embeds[0].Description = out.Reply

	ctx.Reply(discord.ReplyParams{
		Content:    ctx.Menu.Data.(*GptData).Embeds[0],
		Components: ctx.Menu.Data.(*GptData).Components,
		Edit:       true,
	})
}
