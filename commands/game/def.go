package cmd_game

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
	"gitlab.com/tncy-english-project/bot/internal/rapidapi"
)

var DefineCommand = &discord.Command{
	Name:        "define",
	Description: "Le bot va définir.",
	Args: []*discord.Arg{
		{
			Name:        "word",
			Description: "What do you want me to define ?",
			Required:    true,
			Type:        discordgo.ApplicationCommandOptionString,
		},
	},
	Call: define,
}

func define(ctx *discord.CmdContext) {
	// We get the word
	word := ctx.Arguments.GetArg("word", 1, "Hello").Value.(string)

	// We reply to discord saying that we are processing the request
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
		},
	})

	// We define the word
	definition, err := rapidapi.GetDefinition(word)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content:  "An error occured while processing your request :" + err.Error(),
			FollowUp: true,
		})
		return
	}

	// We create the embed
	embed := discordgo.MessageEmbed{
		Title: "Definition of " + word,
		Color: 0x00ff00,
	}

	// We add the pronunciation
	if definition.Ipa != "" {
		embed.Description = "Pronounciation : `" + definition.Ipa + "`"
	}

	// We add the definitions
	if definition.Meaning.Adjective != "" {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:   "Adjective",
			Value:  definition.Meaning.Adjective,
			Inline: false,
		})
	}
	if definition.Meaning.Noun != "" {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:   "Noun",
			Value:  definition.Meaning.Noun,
			Inline: false,
		})
	}
	if definition.Meaning.Verb != "" {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:   "Verb",
			Value:  definition.Meaning.Verb,
			Inline: false,
		})
	}
	if definition.Meaning.Adverb != "" {
		embed.Fields = append(embed.Fields, &discordgo.MessageEmbedField{
			Name:   "Adverb",
			Value:  definition.Meaning.Adverb,
			Inline: false,
		})
	}

	// We create the menu
	discord.ActiveMenus.Set(ctx.ID, &discord.Menus{
		MenuID:        ctx.ID,
		SourceContext: ctx,
		Data:          word,
		Call:          handleDefButton,
	}, 0)

	// We reply to discord with the definition
	ctx.Reply(discord.ReplyParams{
		Content: &embed,
		Components: []discordgo.MessageComponent{
			discordgo.ActionsRow{
				Components: []discordgo.MessageComponent{
					discordgo.Button{
						Label:    "Pronunciation",
						Style:    discordgo.SuccessButton,
						CustomID: ctx.ID,
					},
				},
			},
		},
		FollowUp: true,
	})
}

func handleDefButton(ctx *discord.CmdContext) {
	// We get the word
	word := ctx.Menu.Data.(string)

	// Send this to the TTS command
	ctx.Arguments = []*discord.CommandArg{
		{
			Name:  "text",
			Value: "Here is how I would pronounce it : " + word,
			Found: true,
		},
	}

	// We call the TTS command
	TTSCommand.Call(ctx)
}
