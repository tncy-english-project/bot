package cmd_game

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
	"gitlab.com/tncy-english-project/bot/internal/rapidapi"
)

var ObjectsCommand = &discord.Command{
	Type: discordgo.MessageApplicationCommand,
	Name: "scan",
	Call: scanStart,
}

func scanStart(ctx *discord.CmdContext) {
	// We reply to discord saying that we are processing the request
	ctx.Reply(discord.ReplyParams{
		Content: &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
		},
	})

	msgID := ctx.Interaction.ApplicationCommandData().TargetID

	msg, err := ctx.Session.ChannelMessage(ctx.ChannelID, msgID)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content:  "An error occured while processing your request :" + err.Error(),
			FollowUp: true,
		})
		return
	}

	// We check for attachments
	if len(msg.Attachments) == 0 {
		ctx.Reply(discord.ReplyParams{
			Content:  "No attachments found.",
			FollowUp: true,
		})
		return
	}

	// We check for the first attachment
	attachment := msg.Attachments[0]

	// We check if the attachment is an image
	if attachment.ContentType != "image/png" && attachment.ContentType != "image/jpeg" && attachment.ContentType != "image/jpg" {
		ctx.Reply(discord.ReplyParams{
			Content:  "No image found.",
			FollowUp: true,
		})
		return
	}

	// We check if the attachment is too big
	if attachment.Size > 8*1024*1024 {
		ctx.Reply(discord.ReplyParams{
			Content:  "The image is too big.",
			FollowUp: true,
		})
		return
	}

	// We check if the attachment is too small
	if attachment.Size < 1024 {
		ctx.Reply(discord.ReplyParams{
			Content:  "The image is too small.",
			FollowUp: true,
		})
		return
	}

	// We send the image to the API
	objects, err := rapidapi.GetObjects(attachment.URL)
	if err != nil {
		ctx.Reply(discord.ReplyParams{
			Content:  "An error occured while processing your request :" + err.Error(),
			FollowUp: true,
		})
		return
	}

	// We check if the API found objects
	if len(objects) == 0 {
		ctx.Reply(discord.ReplyParams{
			Content:  "No objects found.",
			FollowUp: true,
		})
		return
	}

	// We create the embed
	embed := &discordgo.MessageEmbed{
		Title:       "Objects found",
		Description: "Here are the objects found in the image :\n\n",
		Color:       0x00ff00,
	}

	// We add the objects to the embed
	for _, object := range objects {
		embed.Description += "**" + object + "**\n"
	}

	// We send the embed
	ctx.Reply(discord.ReplyParams{
		Content:  embed,
		FollowUp: true,
	})
}
