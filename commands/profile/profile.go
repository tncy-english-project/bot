package cmd_profile

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/tncy-english-project/bot/discord"
)

var ProfileCommand = &discord.Command{
	Name:        "profile",
	Description: "Le bot va retourner le profil du joueur.",
	Call:        getProfile,
}

func getProfile(ctx *discord.CmdContext) {
	waPlayed, _ := ctx.User.WAPlayed.Get()
	waPoints, _ := ctx.User.WAPoints.Get()
	wordlePlayed, _ := ctx.User.WordlePlayed.Get()
	wordlePoints, _ := ctx.User.WordlePoints.Get()
	quizPlayed, _ := ctx.User.QuizPlayed.Get()
	quizPoints, _ := ctx.User.QuizPoints.Get()

	embed := &discordgo.MessageEmbed{
		Title:       "Profile of " + ctx.Author.Username,
		Description: "Here is your profile :",
		Color:       0x00ff00,
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:   "Word Association",
				Value:  fmt.Sprintf("You have played **%d** games and collected **%d** points.", waPlayed, waPoints),
				Inline: true,
			},
			{
				Name:   "Wordle",
				Value:  fmt.Sprintf("You have played **%d** games and collected **%d** points.", wordlePlayed, wordlePoints),
				Inline: true,
			},
			{
				Name:   "Quiz",
				Value:  fmt.Sprintf("You have played **%d** games and collected **%d** points.", quizPlayed, quizPoints),
				Inline: true,
			},
			{
				Name:   "Lyrics",
				Value:  fmt.Sprintf("You have collected **%d** points.", quizPoints),
				Inline: true,
			},
		},
	}

	ctx.Reply(discord.ReplyParams{
		Content: embed,
	})
}
